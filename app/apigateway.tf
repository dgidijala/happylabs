# API Gateway
resource "aws_api_gateway_rest_api" "api_gw" {
  name = var.name
  description = "Test API Gateway"
}

# Define a resource within the API for /test/_user_request_/
resource "aws_api_gateway_resource" "test_resource" {
  path_part   = ""
  parent_id   = aws_api_gateway_rest_api.api_gw.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api_gw.id
}

#Define a child resource for /_user_request_/
resource "aws_api_gateway_resource" "user_request_resource" {
  path_part   = "_user_request_"
  parent_id   = aws_api_gateway_resource.test_resource.id
  rest_api_id = aws_api_gateway_rest_api.api_gw.id
}

## Add API key to api gateway
resource "aws_api_gateway_api_key" "helloworld-localstack-test" {
  name = "helloworld-localstack-test"
}

# Define a method for the resource
resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.api_gw.id
  resource_id   = aws_api_gateway_resource.test_resource.id
  http_method   = "ANY"
  authorization = "NONE"
  api_key_required = true # This makes API key mandatory for this method
}

# Create a custom domain name
resource "aws_api_gateway_domain_name" "custom_domain" {
  domain_name = var.api_domain_name
  #certificate_arn = "your_certificate_arn_here" # The ARN of your SSL certificate
}

# Create a base path mapping
resource "aws_api_gateway_base_path_mapping" "custom_domain_mapping" {
  api_id   = aws_api_gateway_rest_api.api_gw.id
  domain_name = aws_api_gateway_domain_name.custom_domain.domain_name
  stage_name = var.stage # The name of the stage you want to map the domain to
}

# Define an integration for the method to invoke the Lambda function
resource "aws_api_gateway_integration" "lambda" {
  rest_api_id             = aws_api_gateway_rest_api.api_gw.id
  resource_id             = aws_api_gateway_resource.test_resource.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this.invoke_arn
}

# Deploy the API
resource "aws_api_gateway_deployment" "apigw_deployment" {
  depends_on = [aws_api_gateway_integration.lambda,]
  rest_api_id = aws_api_gateway_rest_api.api_gw.id
  stage_name  = var.stage
}

resource "aws_lambda_permission" "apigw" {
 statement_id  = "AllowExecutionFromAPIGateway"
 action        = "lambda:InvokeFunction"
 function_name = aws_lambda_function.this.function_name
 principal     = "apigateway.amazonaws.com"
 source_arn = "${aws_api_gateway_rest_api.api_gw.execution_arn}/*/*"
}

# TO FILL IN resources, including but not limited to
# - aws_api_gateway_rest_api
# - aws_api_gateway_method
# - aws_api_gateway_deployment


