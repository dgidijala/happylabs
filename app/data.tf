data "aws_caller_identity" "this" {}

output "account_id" {
  value = data.aws_caller_identity.this.account_id
}

output "caller_arn" {
  value = data.aws_caller_identity.this.arn
}

output "caller_user" {
  value = data.aws_caller_identity.this.user_id
}

data "aws_region" "this" {}
