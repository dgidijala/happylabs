terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.44.0"
      #version = ">=4.57.1"
    }
  }
}
