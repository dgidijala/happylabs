FROM hashicorp/terraform:1.7

## Set environment variables
ENV PATH="${PATH}:/home/myapp-user/.local/bin"
ENV LOCALSTACK_HOSTNAME="localstack"
ENV AWS_ACCESS_KEY_ID="test"
ENV AWS_SECRET_ACCESS_KEY="test"
ENV AWS_DEFAULT_REGION="ap-southeast-1"

# Added - Update APK repositories
RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.14/main" > /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/v3.14/community" >> /etc/apk/repositories

# Reformat - Install necessary packages
RUN apk update && \
    apk add --no-cache \
        python3 \
        py3-pip \
        make \
        curl \
        jq \
    || (echo "Some packages are not available."; exit 1) 

RUN adduser -D -u 1000 myapp-user

# Added -- Install AWS CLI
RUN apk --no-cache add python3 py3-pip && \
    pip3 install --upgrade pip && \
    pip3 install terraform-local awscli 


# Create a symbolic link for the AWS CLI executable
##RUN ln -s ${PATH}/aws /usr/local/bin/aws

# Set AWS credentials and default region
##RUN aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID \
##  && aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY \
##  && aws configure set default.region $AWS_DEFAULT_REGION 

# Set the working directory
WORKDIR /app
# Set AWS configuration (if needed)
# COPY config/.aws /root/.aws

# Copy AWS configuration
COPY config/.aws /home/myapp-user/.aws

# Set entry point
# ENTRYPOINT ["terraform"]
ENTRYPOINT ["tail", "-f", "/dev/null"]
